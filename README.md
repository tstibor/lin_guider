# Description #

Lin-guider is an astronomical autoguiding program for Linux. It supports Philips, Logitech, uvc webcams, QHY5, QHY6, DSI2PRO, QHY5L-II-M, QHY5L-II-C, QHY5-II, ATIK, Starlight Express, ZWO ASI astrocams for video and FTDI chip-based, parallel port-based (LPT), GPIO-based, GPUSB devices, Nexstar-protocol based and QHY5, QHY6, QHY5L-II-M, QHY5L-II-C, QHY5-II, ATIK, Starlight Express, ZWO ASI astrocams for pulse guiding.

Starlight Xpress ltd. is helping this project http://www.sxccd.com/ ￼

# Features #

* Added DSI2PRO support, added linear historamm
* Added QHY6 full support
* Added network notifications and base remote control
* Added new devices QHY5L-II-M, QHY5-II, GPUSB, GPIO, dithering by RPC
* Added support for QHY5L-II-C
* Added support for Nexstar protocol
* Added support of ATIK cameras
* Added 12-bit video mode for QHY5L-II(C,M)
* Added support for ZWO ASI cameras
* Added support for Starlight Express cameras

The code is licensed under [GNU General Public License version 3.0](http://opensource.org/licenses/gpl-3.0.html) and imported from http://sourceforge.net/projects/linguider.